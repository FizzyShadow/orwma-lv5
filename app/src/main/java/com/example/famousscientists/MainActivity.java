package com.example.famousscientists;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button inspiration;
    Button editTextButton;
    RadioButton radioButton1;
    RadioButton radioButton2;
    RadioButton radioButton3;
    TextView textView1;
    TextView textView2;
    TextView textView3;
    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;
    EditText editText;

    ArrayList<String> quotes = new ArrayList<String>();
    String galileoQuote ="“You cannot teach a man anything, you can only help him find it within himself.” - Galileo Galilei";
    String teslaQuote ="“If you want to find the secrets of the universe, think in terms of energy, frequency and vibration.” - Nikola Tesla";
    String einsteinQuote ="“Two things are infinite: the universe and human stupidity; and I'm not sure about the universe.” - Albert Einstein";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        quotes.add(galileoQuote);
        quotes.add(teslaQuote);
        quotes.add(einsteinQuote);
        initializeUI();
    }

    private void initializeUI() {
        this.inspiration = (Button)findViewById(R.id.buttonInspiration);
        this.editTextButton = (Button) findViewById(R.id.buttonEditText);
        this.radioButton1 = (RadioButton)findViewById((R.id.radioButtonOne));
        this.radioButton2 = (RadioButton)findViewById((R.id.radioButtonTwo));
        this.radioButton3 = (RadioButton)findViewById((R.id.radioButtonThree));
        this.textView1 = (TextView)findViewById((R.id.textViewGalileo));
        this.textView2 = (TextView)findViewById((R.id.textViewTesla));
        this.textView3 = (TextView)findViewById((R.id.textViewEinstein));
        this.imageView1 = (ImageView)findViewById(R.id.imageGalileo);
        this.imageView2 = (ImageView)findViewById(R.id.imageTesla);
        this.imageView3 = (ImageView)findViewById(R.id.imageEinstein);
        this.editText = (EditText)findViewById((R.id.editTextOfScientists));

        InspirationOnClick();
        EditTextOnClick();
        RemoveImage();
    }

    private void displayToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private void InspirationOnClick(){
        Random random = new Random();
        inspiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int randomNumber = random.nextInt(quotes.size());
                displayToast(quotes.get(randomNumber));
            }
        });

    }

    private void EditTextOnClick(){
        editTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(radioButton1.isChecked()){
                    textView2.setText(editText.getText());
                }else if(radioButton2.isChecked()){
                    textView1.setText(editText.getText());
                }else if(radioButton3.isChecked()){
                    textView3.setText(editText.getText());
                }else{
                    displayToast("Please select one of the options");
                }
            }

        });

    }

    private void RemoveImage(){
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView1.setVisibility(View.GONE);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView2.setVisibility(View.GONE);
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageView3.setVisibility(View.GONE);
            }
        });
    }

}